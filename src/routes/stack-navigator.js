import {createStackNavigator} from 'react-navigation-stack'
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import Splash from "../scenes/splash/splash";
import Login from '../scenes/login/login';
import Verifqued from '../scenes/login/loginVerifqued'
import Home from '../scenes/home/home'
const stackNavigator = createStackNavigator({
    Login:{
        screen:Login,
        
        navigationOptions:{
            headerMode:"none"
        }
        
    },
    Verifique:Verifqued
},
{
    initialRouteName:'Login',

}
)

const switchNavigator = createSwitchNavigator({
    Splash,
    Login:stackNavigator,
    Home
},
{

})

export default  createAppContainer(switchNavigator)
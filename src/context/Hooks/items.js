
import {useStore} from '../store'
import * as Actions from '../actions'

import services from '../../services/data'
export const useItems = () => {
	const [state, dispatch]  = useStore();

	// List of Props
	const { data } = state;

    // List of Actions
    

    const action2 =()=>{
        
        return new Promise((resolve, reject) => {
            services.balanceList()
                .then(response => {
                    console.log(response)
                 
                    dispatch(Actions.addData(response))
                   
                    console.log(state)
                })
                .catch(error => {
                    console.log(error);
                   
                    reject(error);
                });
        })
    }
    const actions = {
        action2
    }

	return { data, actions };
}

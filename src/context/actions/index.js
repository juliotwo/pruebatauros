import * as types from './types';
import { useStore } from '../store'
export const addTodo = todo =>
    (
        {
            type: types.ADD_TODO,
            payload: { todo }
        }
    );

export const addData = payload => ({ type: types.ADD_DATA, payload });


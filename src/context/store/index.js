import React, { createContext, useContext, useReducer } from 'react';

export const StateContext = createContext();
import rootReducer from '../reducers'
import initialState from './initialState'

const combineReducers = reducers => (state, action) => {
  let hasChanged;
  const nextState = Object.keys(reducers).reduce((result, key) => {
    result[key] = reducers[key](state[key], action);
    hasChanged = hasChanged || result[key] !== state[key];
    return result;
  }, {});
  return hasChanged ? nextState : state;
};



export const StoreProvider = ({ children }) => (
  
  <StateContext.Provider
    value={useReducer(rootReducer, initialState)}
    
  >
    {children}
  </StateContext.Provider>
);

export const useStore = () => useContext(StateContext);
export const add = (state, { payload }) => ({
  ...state,
  todos: [...state.todos, payload.todo],
});

export const addData = (state, payload) => ({
  ...state,
  data: payload.payload.data,
});


import * as todo from './todo';
import * as types from '../actions/types';

const createReducer = handlers => (state, action) => {
  if (!handlers.hasOwnProperty(action.type)) {
    return state;
  }

  return handlers[action.type](state, action);
};

export default createReducer({
  [types.ADD_TODO]: todo.add,
  [types.ADD_DATA]: todo.addData,

});
import React, { useState } from "react";
import { View, Text, StyleSheet, ImageBackground, Dimensions, Image, TouchableOpacity, Platform } from 'react-native';
import { Input, Button } from 'react-native-elements';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import logo from '../../assets/tauros.jpg';
import LoginService from '../../services/login'
import SnackBar2 from '../../utils/snackbar'
import Snackbar from '@prince8verma/react-native-snackbar';
import {HttpInterceptor} from '../../utils/http-interceptor'
const isAndroid = Platform.OS === 'android';


export default  (props) =>  {
    const [code,setCode] = useState("")
    const token = props.navigation.state.params.payload.token
    console.log("props verifi",token)

    const login = ()=>{
        LoginService.loginVerifiqued({token:token,code:code})
        .then((res)=>{
            console.log("respuesta ",res)
            if (!res.success){
                console.log("credenciales no validas")
                SnackBar2.error("Credenciales no validas")
            }
            else{
                console.log("credencialesvalidas")
                SnackBar2.success("Credenciales validas")
                HttpInterceptor.registerInterceptor({idToken:res.payload.token})
                HttpInterceptor.navigator = props.navigation
                props.navigation.navigate('Home')

            }
            
        })

    }
    return (
        <View style={styles.container}>
            <KeyboardAwareScrollView>
                <View style={styles.logoContainer}>
                    <Image style={styles.logo} source={logo} resizeMode='contain' />
                </View>
                <View style={styles.formContainer}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Input
                            onChangeText={value => setCode(value)}
                            label = 'Usuario'
                            labelStyle = {{fontSize:16}}
                            allowFontScaling={false}
                            autoCapitalize="none"
                            keyboardType="number-pad"
                            onSubmitEditing = {()=>inputPassword.focus()}
                            placeholder='Codigo de verificacion'
                            placeholderTextColor="#000"
                            inputStyle={styles.inputStyle}
                            containerStyle={styles.input}
                            inputContainerStyle={{ borderWidth:1, borderColor:'blue',marginTop:10,borderRadius:10 }} />
                    </View>
                    <View style={styles.screen}>
                        <Button
                            onPress={()=>{login()}}
                            loadingProps={{ size: 'small' }}
                            containerStyle={styles.row}
                            titleStyle={{ color: '#000', fontWeight: '600', fontSize: 16 }}
                            buttonStyle={[styles.buttonFB]}
                            title={'Verificar'} />
                    </View>
                </View>
                <Snackbar id={"root_app"} />

            </KeyboardAwareScrollView>
        </View>
    )
}
const { width, height } = Dimensions.get('window');


const THEME_PALETTE = {
    PRIMARY: '#273746',
    PRIMARY_DARK: '#1C2833',
    SECONDARY: '#1ABC9C',
    SECONDARY_DISABLED: '#76D7C4',
    FACEBOOK: '#3B5999',
    GOOGLE: '#DD4B39',
    ERROR: '#D50A2F',
    MUTED: '#9B999C',
    GRAY: '#B3B6B7'
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        paddingHorizontal: 20,
        paddingTop: 30
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    container: {
        flex: 1,
        paddingTop: isAndroid ? 10 : height * 0.1,
        width: '100%',
        height: '100%',
        backgroundColor: '#EFFBF8'

    },
    logoContainer: {
        flex: 3,
        justifyContent: 'center',
        alignItems: 'center',
        overflow: 'visible'
    },
    logo: {
        width: width * 0.5,
        height: width * 0.5,
        overflow: 'visible'
    },
    formContainer: {
        flex: 4,
        alignItems: 'center',
        paddingHorizontal: 30,
        paddingTop: height * 0.05
    },
    containerOthers: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'transparent',
        flexDirection: 'column',
        alignItems: 'center'
    },
    passwordContainer: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'transparent'
    },
    registerContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent'
    },
    text1: {
        padding: 20,
        color: '#000',
        textAlign: 'center',
    },
    text2: {
        padding: 20,
        color: '#000',
        textDecorationLine: 'underline'
    },
    text3: {
        color: '#000',
        textAlign: 'center',
        fontSize: 15
    },
    icon: {
        paddingRight: 10
    },
    label: {
        fontWeight: 'normal',
        color: '#000',
        fontSize: 12,
        marginLeft: 10
    },
    input: {
        marginBottom: 15,
     
    },
    inputStyle: {
        height: isAndroid ? undefined : 45,
        color: '#000000',
        fontSize: 18,
        textAlign: 'center'
    },
    button: {
        height: 35,
        borderRadius: 20
    },
    row: {
        marginBottom: 25
    },
    buttonFB: {
        backgroundColor: THEME_PALETTE.GOOGLE,
        width: '100%',
        borderColor: "transparent",
        borderWidth: 0,
        elevation: 0,
        height: 35,
        borderRadius: 20
    }
});

import { StyleSheet } from 'react-native';


export default StyleSheet.create({
    screen: {
        flex: 1,
        paddingHorizontal: 30,
        paddingVertical: 30,
        backgroundColor:'white',
        justifyContent: 'center',
        alignItems: 'center',
    },
    center:{
        
    }
});
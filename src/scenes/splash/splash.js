import React, { Component,useEffect } from "react";
import { View, Text,Image,Dimensions } from "react-native";
import Styles from "./splashStyles";
import logo from "../../assets/tauros.jpg";
const SplashScreen = (props) => {
    useEffect(() => {
        console.log("mount")
    
        // Especifica cómo sanear este efecto:
        return function cleanup() {
            console.log("unmount")
        };
      });
    setTimeout(()=>{
        console.log("pasaron 3 segundos");
        props.navigation.navigate('Login')
    },3000)
    console.log("splashProps",props)
    const { width, height} = Dimensions.get('screen')
    const imageSize = (height > width ? height : width) * .30;
    return (
        <View style={Styles.screen}>
            
            <Image style={{height:imageSize,width:imageSize}} resizeMode="contain" source={logo} ></Image>
            
        </View>
    )
};


export default SplashScreen
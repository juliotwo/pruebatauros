import React,{useEffect} from 'react';

import { useItems } from '../../context/Hooks/items';

import { Text, Button , SafeAreaView, View, FlatList, StyleSheet,Image} from 'react-native';

export default () => {
  const {data,actions} = useItems();
  console.log(data,actions)
  const getAll = () =>{
    actions.action2().then((res) =>{
        if(res.succes){
            console.log("res homea",data)
        }
        else{
            console.log("error al trer la informacion")
        }
    })
  }
  

  useEffect(() => {
    console.log("mount")
    getAll();
    return function cleanup() {
        console.log("unmount")
    };
  },[]);
    
  return (
    <SafeAreaView>
    <FlatList
      data={data.wallets ? data.wallets:null}
      renderItem={({ item }) => <Item datos={item} />}
    />
  </SafeAreaView>
  );
};

function Item({ datos }) {
    return (
      <View style={styles.container}>
          <View style={styles.section}>
               <Image style={styles.logo} source={{uri: datos.coin_icon}} resizeMode='contain' />

          </View>
          <View style={styles.section2}>
            <Text style={styles.title}>{datos.coin}</Text>
            <Text>{datos.coin_name}</Text>

          </View>
          <View style={styles.section3}>
              <Text style={styles.title}>
                  {datos.balances.available}
              </Text>
          </View>
      </View>
    );
  }
  
 
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      marginTop: 10,
      marginBottom:20,
      flexDirection:"row",
      backgroundColor: '#f9c2ff',
      padding: 20,
      marginVertical: 8,
      marginHorizontal: 16,
      height:120
    },

    title: {
      fontSize: 22,
    },
    logo: {
        width: 20,
        height: 20,
        overflow: 'visible'
    },
    section:{
        flex:1,
        justifyContent:"center",
        alignContent:"center"
    },
    section2:{
        flex:2,
        justifyContent:"center",
        alignContent:"center"
    },
    section3:{
        flex:2,
        justifyContent:"center",
        alignContent:"center"
    }
  });
  
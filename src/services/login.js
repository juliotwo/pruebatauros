import environment from '../enviroment';

export default class LoginService {

 
    static async login(payload) {
        console.log('servicio', payload);
        console.log(`${environment.ENDPOINT}/api/v2/auth/signin/`)
        return fetch(`${environment.ENDPOINT}/api/v2/auth/signin/`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: payload.email,
                password: payload.password,
                unique_device_id: 987654321,
                device_name: "Prueba"
            })
        }).then(response => {
            return response.json()
        })
        .then(body => {
            if (body.message === 'Unauthorized')
                throw body.error;

            return body;
        });
    }
    static async loginVerifiqued(payload) {
        console.log('servicio', payload);
        console.log(`${environment.ENDPOINT}/api/v2/auth/verify-tfa-email/ `)
        return fetch(`${environment.ENDPOINT}/api/v2/auth/verify-tfa-email/ `, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                tempToken: payload.token,
                code: payload.code,
                unique_device_id: 987654321,
                device_name: "Prueba"
            })
        }).then(response => {
            return response.json()
        })
        .then(body => {
            if (body.message === 'Unauthorized')
                throw body.error;

            return body;
        });
    }
}
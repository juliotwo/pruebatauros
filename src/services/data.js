import environment from '../enviroment';

export default class LoginService {

 
    static async balanceList() {
        console.log('servicioData');
       
        return fetch(`https://api.staging.tauros.io/api/v1/data/listbalances/`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => {
            return response.json()
        })
        .then(body => {
            if (body.message === 'Unauthorized')
                throw body.error;

            return body;
        });
    }

}
import fetchIntercept from 'fetch-intercept';
import environment from '../enviroment';
import Snackbar from '../utils/snackbar';

export class HttpInterceptor {
    static unregister;

    static navigator;

    static registerInterceptor(credentials) {
        
        if (this.unregister)
            this.unregister();

        this.unregister = fetchIntercept.register({
            request: (url, config) => {
                if (credentials){         
                    const authorization = url.includes(environment.ENDPOINT) ? url.includes('api/v1/data') ? `JWT ${credentials.idToken}` : `JWT ${credentials.token}` : credentials.token;
                    // console.log('Authorization:   ',  authorization); 
                    config = { ...config, headers: { ...(config ? config.headers : {}), 'Authorization': authorization } };
                }
                // console.log(url, config)
                return [url, config];
            },
            requestError: error => {
                // Called when an error occurred during another 'request' interceptor call
                console.log(error);
                return Promise.reject(error);
            },

            response: response => {
                // Modify the response object
                if(response.status === 401){
                    Snackbar.error('Tu sesión ha expirado.');
                    this.navigator._navigation.navigate('Onboarding');
                    return Promise.reject(401);
                }

                return response;
            },

            responseError: function (error) {
                // Handle an fetch error
                console.log(error);
                return Promise.reject(error);
            }
        });
    }
}

HttpInterceptor.registerInterceptor();

import { showSnackBar } from '@prince8verma/react-native-snackbar';
import { Platform } from 'react-native';

const isAndroid = Platform.OS === 'android';

export default class Snackbar {

    static success(message) {
        showSnackBar({
            message,
            textColor: '#FFF',      // message text color
            position: isAndroid ? 'top' : 'bottom',  // enum(top/bottom).
            confirmText: null, // button text.
            buttonColor: '#03a9f4', // default button text color
            duration: 3000,   // (in ms), duartion for which snackbar is visible.
            animationTime: 250, // time duration in which snackbar will complete its open/close animation.
            backgroundColor: "#323232", //background color for snackbar
        });
    }

    static error(message) {
        showSnackBar({
            message,
            textColor: '#FFF',      // message text color
            position: isAndroid ? 'top' : 'bottom',  // enum(top/bottom).
            confirmText: null, // button text.
            buttonColor: '#03a9f4', // default button text color
            duration: 3000,   // (in ms), duartion for which snackbar is visible.
            animationTime: 250, // time duration in which snackbar will complete its open/close animation.
            backgroundColor: "red", //background color for snackbar
            onConfirm: () => { },    //  perform some task here on snackbar button press.
        });
    }
}
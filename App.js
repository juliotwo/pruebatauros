/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import MainApp from './src/routes/stack-navigator'
import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons'

Icon.loadFont();

const App = (props) => {
  console.log("Props main",props)
  return (
    <>
      <MainApp>

      </MainApp>
    </>
  );
};



export default App;
